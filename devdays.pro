TARGET = qdevdaysgraphicssystem

TEMPLATE = lib
CONFIG += plugin
QT += network

SOURCES = main.cpp qgraphicssystem_dd.cpp
HEADERS = qgraphicssystem_dd.h protocol.h

target.path += $$[QT_INSTALL_PLUGINS]/graphicssystems
INSTALLS += target
