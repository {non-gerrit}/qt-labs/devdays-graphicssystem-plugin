/****************************************************************************
**
** Copyright (C) 2008-2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the Itemviews NG project on Trolltech Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QGRAPHICSSYSTEM_DD_H
#define QGRAPHICSSYSTEM_DD_H

#include <QtCore/qhash.h>
#include <QtCore/qsharedmemory.h>
#include <QtNetwork/qtcpsocket.h>

#include <QtGui/private/qgraphicssystem_p.h>
#include "protocol.h"

QT_BEGIN_NAMESPACE

class QDevDaysGraphicsSystem;
class QDevDaysGraphicsSystemScreen;

class QDevDaysWindowSurface : public QWindowSurface
{
public:
    QDevDaysWindowSurface(QWidget *window, QDevDaysGraphicsSystem *system);
    ~QDevDaysWindowSurface();

    QPaintDevice *paintDevice();
    void flush(QWidget *widget, const QRegion &region, const QPoint &offset);
    void setGeometry(const QRect &rect);
    bool scroll(const QRegion &area, int dx, int dy);

    void beginPaint(const QRegion &region);
    void endPaint(const QRegion &region);
    void setVisible(bool visible);
    void raise();
    void lower();
    Qt::WindowFlags setWindowFlags(Qt::WindowFlags type);
    Qt::WindowFlags windowFlags() const;

private:
    QDevDaysGraphicsSystem *m_system;
    quint32 m_id;
    QSharedMemory m_shared;
    QImage m_image;
    Qt::WindowFlags window_flags;
};

class QDevDaysGraphicsSystemScreen : public QGraphicsSystemScreen
{
public:
    QDevDaysGraphicsSystemScreen() {}
    ~QDevDaysGraphicsSystemScreen() {}

    QRect geometry() const { return QRect(0, 0, 640, 480); } // size in pixels
    int depth() const { return 32; }
    QImage::Format format() const { return QImage::Format_ARGB32; }
    QSize physicalSize() const { return QSize(80, 108); } // size in milimeters
};

class QDevDaysGraphicsSystem : public QObject, public QGraphicsSystem
{
    Q_OBJECT
public:
    QDevDaysGraphicsSystem();

    // QGraphicsSystem interface
    QPixmapData *createPixmapData(QPixmapData::PixelType type) const;
    QWindowSurface *createWindowSurface(QWidget *widget) const;
    QList<QGraphicsSystemScreen*> screens() const;

    // client interface
    bool connectToServer();
    bool sendRequest(const Request &request);
    bool waitForResponse(Response &response);

public slots:
    void eventDispatcher();

private:
    QTcpSocket m_connection;
    Message m_message;

public:
    mutable QHash<quint32, QDevDaysWindowSurface*> m_surfaces;
    mutable QHash<QWidget*, quint32> m_ids;
    mutable QDevDaysGraphicsSystemScreen m_screen;
    //mutable Client m_client;
};


QT_END_NAMESPACE

#endif
